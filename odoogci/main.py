import sys
import os
import subprocess
import argparse
import uuid
VERSION="0.1.5"
ABS_PATH = os.path.join(os.path.dirname(__file__))
def install_req(repo_path, archive='requirements.txt'):
    """Instala los requerimientos usando pip
    
    Keyword arguments:
    archive -- archivo que tiene las dependencias. Por defecto requirements.txt
    Return: Si se instaló correctamente devuelve True, caso contrario False
    """
    
    archive_abs_path = f"{repo_path}/{archive}"
    if os.path.exists(archive_abs_path):

        print("Instalando dependencias ", end=' ')
        try:
            command = f"pip install -r {archive_abs_path}"
            pip_install = subprocess.run(command, shell=True, check=True)
            print("[OK]")
        except:
            print("[ERROR]")
    else:
        print("No hay archivo de dependencias[OK]")

def remove_garbage(repo_path):
    """Remueve archivos innecesarios
    
    Return: None
    """
    
    items = [
        'LICENSE',
        '*.md',
        '*.txt',
        '.git'
    ]
    print("Removiendo archivos innecesarios ", end=' ')
    items = os.listdir(f"{repo_path}/")
    try:
        ## Remover archivos
        for item in items:
            if os.path.isfile(f"{repo_path}/{item}"):
                command = f"rm -rf {repo_path}/{item}"
                remove = subprocess.run(command,  shell=True, check=True)

        ## Remover datos de repositorio Git
        command = f"rm -rf {repo_path}/.git"        
        remove = subprocess.run(command,  shell=True, check=True)
        ## Remover carpeta setup
        command = f"rm -rf {repo_path}/setup"
        remove_setup = subprocess.run(command,  shell=True, check=True)

        print("[OK]")
    except:
        print("[ERROR]")
        sys.exit(1)

def move_folders(repo_path):
    """Mueve todas las carpetas del repositorio al directorio y borrar el repositorio padre
    
    """
    print("Moviendo repositorio a carpeta actual ", end=' ')
    try:
        command = f"mv {repo_path}/* ."
        move_folders = subprocess.run(command, shell=True, check=True)
        command = f"rm -rf {repo_path}"
        delete_repo = subprocess.run(command, shell=True, check=True)
        print("[OK]")
    except:
        print("[ERROR]")
        sys.exit(1)

def main():
    """Utilidad para clonar repositorios git, eliminar archivos innecesarios e
    instalar dependencias de requirements.txt. Enfocada al uso como helper
    para el armado de imágenes de Odoo

    Keyword arguments:
    url -- URL del repositorio 
    branch -- Rama que se desea clonar
    """
    parser = argparse.ArgumentParser(description="Utilidad para clonar repositorios git, borrar archivos innecesarios e instalar requerimientos")
    parser.add_argument('-u','--url',type=str, help='URL https del repositorio')
    parser.add_argument('-b','--branch',type=str, help='Rama a clonar')
    parser.add_argument('-t','--test', action='store_true', help='Clonar con limpieza pero sin instalar dependencias')
    parser.add_argument('-v','--version', action='store_true', help='Devuelve versión de la herramienta')

    args = parser.parse_args()
    if not any(vars(args).values()):
        parser.print_help()
        sys.exit(1)
    if args.version:
        print(VERSION)
        sys.exit(0)
    if args.url is None:
        print("El parámetro url es obligatorio")
        sys.exit(1)
    if args.branch is None:
        print("El parámetro branch es obligatorio")
        sys.exit(1)
    #1. Se clona el repositorio
    print (f"Clonando {args.url} rama {args.branch} ", end=' ')
    repo_path = uuid.uuid4().hex
    command = f"git clone {args.url} -b {args.branch} {repo_path}"
    #repo_path=args.url.split("/")[4]

    try:
        gitclone = subprocess.run(command,  shell=True, check=True)
        print("[OK]")
    except:
        print(f"[ERROR]")
        # sys.exit(1)
        
    #2. Instalar requirements.txt
    if not args.test:
        install_req(repo_path=repo_path)

    #3. Borrar archivos innecesarios
    remove_garbage(repo_path=repo_path)
    #4. Mover carpetas
    move_folders(repo_path=repo_path)

if __name__ =='__main__':
    main()
