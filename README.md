# Git Repository Helper Script

Este script es una utilidad para clonar repositorios de Git, eliminar archivos innecesarios e instalar dependencias desde un archivo `requirements.txt`. Está enfocado en facilitar el armado de imágenes de Odoo.

## Requisitos

Asegúrate de tener las siguientes herramientas instaladas en tu sistema:

- Python ^3.9
- Git
- pip (gestor de paquetes de Python)

## Instalación
    1. Dentro de su Dockerfile agregar la siguiente la línea:
```Dockerfile
RUN pip3 install git+https://gitlab.com/alitux/odoogci
```

## Uso
El uso general de la herramienta es el siguiente:
```sh
odoogci -u <URL del repositorio> -b <Nombre de la rama>
```
Si bien se puede usar de forma externa, la utilidad está diseñada para ser **utilizada dentro de un Dockerfile**.

### Ejemplo de uso en un Dockerfile

```Dockerfile
RUN cd /usr/lib/python3/dist-packages/odoo/addons/ &&\                                                                                      
    odoogci -u "https://github.com/ingadhoc/odoo-argentina" -b "16.0" &&\                                                                   
    odoogci -u "https://github.com/ingadhoc/account-invoicing" -b "16.0"
```

